Symfony Beubi Skeleton
========================

This is a Symfony 2 skeleton project. To start a new project you just need to copy this whole directory and
find all occurences of the string 'sf2-project' inside the chef directory, Berksfile and Vagrantfile, and replace
them by the new project name.

Just to test how the system behaves with a new pull request.